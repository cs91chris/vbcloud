FROM python:3.8

COPY requirements.txt .

RUN pip3 install --user -r requirements.txt

ENV PYTHONDONTWRITEBYTECODE=1
ENV PATH=/root/.local/bin:$PATH
ENV PYTHONPATH=/root/.local:$PYTHONPATH

WORKDIR /app

COPY resource/run.sh .
COPY vbcloud ./vbcloud
COPY resource/config ./config

EXPOSE 5000
