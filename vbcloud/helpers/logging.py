# noinspection PyMethodMayBeStatic
class DummyLogger:
    def debug(self, *args):
        print(*args)

    def info(self, *args):
        print(*args)

    def warning(self, *args):
        print(*args)

    def error(self, *args):
        print(*args)

    def exception(self, *args):
        print(*args)
