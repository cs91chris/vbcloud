import requests
from requests.auth import AuthBase, HTTPBasicAuth

from vbcloud.helpers.logging import DummyLogger
from . import httpcode
from .dumper import HTTPDumper


class HTTPTokenAuth(AuthBase):
    def __init__(self, token):
        """

        :param token:
        """
        self.token = token

    def __eq__(self, other):
        """

        :param other:
        :return:
        """
        return self.token == getattr(other, 'token', None)

    def __ne__(self, other):
        """

        :param other:
        :return:
        """
        return not self == other

    def __call__(self, r):
        """

        :param r:
        :return:
        """
        r.headers['Authorization'] = "Bearer {}".format(self.token)
        return r


class HTTPBase(HTTPDumper):
    def __init__(self, dump_body=False, raise_on_exc=False, logger=None):
        """

        :param dump_body:
        :param raise_on_exc:
        :param logger:
        """
        self._dump_body = dump_body
        self._raise_on_exc = raise_on_exc
        self._logger = logger or DummyLogger()

    def request(self, uri, dump_body=None, **kwargs):
        """

        :param uri:
        :param dump_body:
        :param kwargs:
        """
        raise NotImplemented


class HTTPClient(HTTPBase):
    def __init__(self, endpoint=None, token=None, username=None, password=None,
                 raise_on_exc=False, dump_body=False, logger=None):
        """

        :param endpoint:
        :param token:
        :param username:
        :param password:
        :param dump_body:
        :param raise_on_exc:
        :param logger:
        """
        super().__init__(dump_body, raise_on_exc, logger)
        self._endpoint = endpoint or ""
        self._username = username
        self._password = password
        self._token = token

        if not self._endpoint.startswith('http'):
            self._endpoint = "http://{}".format(self._endpoint)

    def request(self, uri="", method='GET', dump_body=None, **kwargs):
        """

        :param method:
        :param uri:
        :param dump_body:
        :param kwargs:
        :return:
        """
        if self._username and self._password:
            kwargs['auth'] = HTTPBasicAuth(self._username, self._password)
        elif self._token:
            kwargs['auth'] = HTTPTokenAuth(self._token)

        try:
            url = "{}{}".format(self._endpoint, uri)
            response = requests.request(method, url, **kwargs)
            self._logger.info(self.dump_request(response.request))
        except (requests.ConnectionError, requests.Timeout) as exc:
            self._logger.exception(exc)
            if self._raise_on_exc:
                raise

            return dict(
                body={},
                status=httpcode.SERVICE_UNAVAILABLE,
                headers={},
                exception=exc
            )

        try:
            response.raise_for_status()
        except requests.HTTPError as exc:
            self._logger.warning(self.dump_response(response, dump_body))
            response = exc.response
            if self._raise_on_exc:
                raise
        else:
            self._logger.info(self.dump_response(response, dump_body))

        try:
            body = response.json()
        except ValueError as exc:
            self._logger.debug(str(exc))
            body = response.text

        return dict(
            body=body,
            status=response.status_code,
            headers=dict(response.headers)
        )

    def get(self, uri="", **kwargs):
        """

        :param uri:
        :param kwargs:
        :return:
        """
        return self.request(uri, **kwargs)

    def post(self, uri="", **kwargs):
        """

        :param uri:
        :param kwargs:
        :return:
        """
        return self.request(uri, 'POST', **kwargs)

    def put(self, uri="", **kwargs):
        """

        :param uri:
        :param kwargs:
        :return:
        """
        return self.request(uri, 'PUT', **kwargs)

    def patch(self, uri="", **kwargs):
        """

        :param uri:
        :param kwargs:
        :return:
        """
        return self.request(uri, 'PATCH', **kwargs)

    def delete(self, uri="", **kwargs):
        """

        :param uri:
        :param kwargs:
        :return:
        """
        return self.request(uri, 'DELETE', **kwargs)
