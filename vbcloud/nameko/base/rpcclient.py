from nameko.standalone.rpc import ClusterRpcProxy

from nameko import config as default_config


class RPCProxyClient:
    config = default_config

    @classmethod
    def perform(cls, client, service, method, background=False, **params):
        """

        :param client:
        :param service:
        :param method:
        :param background:
        :param params:
        :return:
        """
        perform = getattr(client[service], method)
        if background is True:
            return perform.call_async(**params)
        else:
            return perform(**params)

    @classmethod
    def invoke(cls, *args, **kwargs):
        with ClusterRpcProxy(cls.config) as client:
            return cls.perform(client, *args, **kwargs)

    @classmethod
    def multiple_invoke(cls, actions):
        """

        :param actions: a list of rpc invokation object, like:
            [{
                'service': 'service',
                'method': 'method',
                'params':  {},  # optional dict
                'background': True  # optional bool
            }]
        :return:
        """
        with ClusterRpcProxy(cls.config) as client:
            for a in actions:
                return cls.perform(
                    client,
                    a['service'],
                    a['method'],
                    a.get('background') or False,
                    **(a.get('params') or {})
                )
