import logging

from nameko import config as global_config
from nameko_tracer import Tracer
from vbcloud import __version__


class BaseService:
    """
    base class that every service should extend
    """
    # default version: SHOULD be override by subclass
    version = __version__

    # service name: MUST be override by subclass
    name = "base_service"

    # nameko global config
    config = global_config

    # every service has its own logger
    logger = logging.getLogger(name)

    # common extensions
    tracer = Tracer()
