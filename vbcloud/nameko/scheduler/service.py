from nameko.web.handlers import http

from vbcloud.helpers.http import httpcode
from .provider import SchedulerProvider


# noinspection PyUnresolvedReferences,PyUnusedLocal
class SchedulerService:
    name = "scheduler_service"
    scheduler = SchedulerProvider(name)

    @http('POST', '/start')
    def start(self, request):
        self.scheduler.start(), httpcode.NO_CONTENT

    @http('POST', '/pause/<job_id>')
    def pause_job(self, request, job_id):
        self.scheduler.pause_job(job_id), httpcode.NO_CONTENT

    @http('POST', '/pause')
    def pause(self, request):
        self.scheduler.pause(), httpcode.NO_CONTENT

    @http('POST', '/resume/<job_id>')
    def resume_job(self, request, job_id):
        self.scheduler.resume_job(job_id), httpcode.NO_CONTENT

    @http('POST', '/resume')
    def resume_job(self, request):
        self.scheduler.resume(), httpcode.NO_CONTENT

    @http('GET', '/jobs/<job_id>')
    def jobs(self, request, job_id):
        job = self.scheduler.get_job(job_id)
        if job:
            return self.dict_job(job)
        else:
            return None, httpcode.NOT_FOUND

    @http('GET', '/jobs')
    def jobs(self, request):
        return [self.dict_job(j) for j in self.scheduler.get_jobs()]

    @staticmethod
    def dict_job(job):
        return {
            'id':                 job.id,
            'name':               job.name,
            'kwargs':             job.kwargs,
            'coalesce':           job.coalesce,
            'trigger':            str(job.trigger),
            'executor':           job.executor,
            'max_instances':      job.max_instances,
            'misfire_grace_time': job.misfire_grace_time,
            'next_run_time':      job.next_run_time.isoformat()
        }
