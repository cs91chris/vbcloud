from apscheduler.schedulers.background import BackgroundScheduler
from nameko.extensions import DependencyProvider

from vbcloud.nameko.base.rpcclient import RPCProxyClient


class SchedulerProvider(DependencyProvider, RPCProxyClient):
    def __init__(self, name, func=None, auto_start=True):
        """

        :param name:
        :param func:
        """
        self.jobs = []
        self.name = name
        self.func = func
        self.auto_start = auto_start
        self.scheduler = None
        self.config = self.config[name]

    def setup(self):
        self.scheduler = BackgroundScheduler(self.config['scheduler'] or {})
        for j in self.config['jobs']:
            self.scheduler.add_job(func=self.func or self.invoke, **j)

    def start(self):
        if self.auto_start is True:
            self.scheduler.start()

    def get_dependency(self, worker_ctx):
        return self.scheduler
