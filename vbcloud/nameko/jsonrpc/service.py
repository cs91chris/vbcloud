import json

from nameko.web.handlers import http

from ..base import BaseService, RPCProxyClient


class JsonRPCProxyService(BaseService, RPCProxyClient):
    name = "jsonrpc_service"
    uri = '/jsonrpc'

    @http('POST', uri)
    def perform(self, request):
        """

        :param request:
        :return:
        """
        data = json.loads(request.get_data(as_text=True))
        params = data.get('params') or {}
        service, method = data['method'].split('.', 2)
        return self.invoke(service, method, 'id' not in data, **params)
