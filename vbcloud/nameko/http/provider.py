from nameko.extensions import DependencyProvider

from vbcloud.helpers.http import HTTPClient


class HTTPClientProvider(DependencyProvider):
    def get_dependency(self, worker_ctx):
        return HTTPClient
