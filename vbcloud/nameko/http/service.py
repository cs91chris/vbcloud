from nameko.rpc import rpc

from vbcloud.helpers import json
from .provider import HTTPClientProvider
from ..base import BaseService


# noinspection PyCallingNonCallable
class HTTPClientService(BaseService):
    name = "httpclient_service"

    client = HTTPClientProvider()

    @rpc
    def perform(self, request, as_json=False, **kwargs):
        """

        :param request:
        :param as_json:
        :param kwargs:
        :return:
        """
        endpoint = request.pop('endpoint')
        auth = request.pop('auth', None) or {}
        client = self.client(endpoint, **auth, **kwargs)
        response = client.request(**request)

        if as_json is True:
            return json.dumps(response)
        return response

    @rpc
    def get(self, request, **kwargs):
        kwargs['method'] = "GET"
        return self.perform(request, **kwargs)

    @rpc
    def post(self, request, **kwargs):
        kwargs['method'] = "POST"
        return self.perform(request, **kwargs)

    @rpc
    def put(self, request, **kwargs):
        kwargs['method'] = "PUT"
        return self.perform(request, **kwargs)

    @rpc
    def delete(self, request, **kwargs):
        kwargs['method'] = "DELETE"
        return self.perform(request, **kwargs)
