from nameko.rpc import rpc
from nameko_sqlalchemy import DatabaseSession
from sqlalchemy.ext.declarative import declarative_base

from .resolver import SQLResolver
from ..base import BaseService


# noinspection PyCallingNonCallable
class SQLExecutorService(BaseService):
    name = "sqlexecutor_service"

    resolver = SQLResolver()
    db = DatabaseSession(declarative_base())

    @rpc
    def execute(self, statement=None, params=None):
        """

        :param statement:
        :param params:
        :return:
        """
        rs = self.db.execute(self.resolver(statement, params or {}))
        return [dict(r) for r in rs]
