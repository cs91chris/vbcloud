from nameko.extensions import DependencyProvider


class SQLResolver(DependencyProvider):
    @classmethod
    def escape(cls, param):
        return param.replace("'", "''")

    @classmethod
    def parse(cls, param):
        if param is None:
            return "NULL"
        if type(param) is not str:
            return param
        try:
            if param.startswith('0') and len(param) > 1:
                value = cls.escape(param)
            else:
                value = int(param)
        except (ValueError, TypeError):
            try:
                value = float(param)
            except (ValueError, TypeError):
                value = cls.escape(param)

        return value

    @classmethod
    def resolve(cls, statement, params=None, parse=True):
        for k in params.keys():
            value = cls.parse(params[k]) if parse else params[k]
            statement = statement.replace('@{{' + k + '}}', str(value))

        return statement

    def get_dependency(self, worker_ctx):
        return self.resolve
