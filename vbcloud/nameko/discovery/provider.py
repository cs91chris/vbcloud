import re

from nameko.extensions import DependencyProvider
from pyrabbit.api import Client


class ServiceDiscoveryProvider(DependencyProvider):
    prefix = 'rpc-'
    suffix = '_service'
    queue_regex = re.compile(r'{}\w+{}'.format(prefix, suffix))

    def __init__(self, host, username=None, password=None, timeout=5, vhost=None, regex=None):
        """

        :param host:
        :param username:
        :param password:
        """
        self.host = host
        self.username = username
        self.password = password
        self.timeout = timeout
        self.vhost = vhost
        self.regex = regex or self.queue_regex
        self.client = None

    def setup(self):
        self.client = Client(self.host, self.username, self.password, self.timeout)

    def find_services(self, name=None, vhost=None):
        """
        Returns a list of service names which are available on the network

        :param name: service name
        :param vhost: rabbitmq virtual host
        """
        resp = []
        if name:
            queue_name = "{}{}{}".format(self.prefix, name, self.suffix)
            queue = self.client.get_queue(vhost or self.vhost or '/', queue_name)
            return dict(queue)

        for q in self.client.get_queues(vhost or self.vhost):
            if self.is_nameko_service(q["name"]):
                key = q["name"].replace(self.prefix, "")
                resp.append({key: dict(q)})

        return resp

    def is_nameko_service(self, name, regex=None):
        """
        Checks if queue name matches pattern used by nameko for service queues

        :param name: queue name
        :param regex: service name pattern
        """
        regex = regex or self.regex
        if regex is not None:
            return bool(regex.search(name))
        return name.startswith(self.prefix) and name.endswith(self.suffix)

    def get_dependency(self, worker_ctx):
        return self.find_services
