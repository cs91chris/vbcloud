from nameko.rpc import rpc
from nameko.web.handlers import http

from vbcloud.helpers import json
from .provider import ServiceDiscoveryProvider
from ..base import BaseService


# noinspection PyCallingNonCallable
class DiscoveryRPCService(BaseService):
    name = 'discovery_rpc_service'
    uri = '/services'
    management_config = BaseService.config.get(name, {}).get('management', {})
    discovery = ServiceDiscoveryProvider(**management_config)

    @rpc
    def services(self, name=None, vhost=None):
        """

        :param name:
        :param vhost:
        :return:
        """
        return self.discovery(name, vhost)


# noinspection PyCallingNonCallable
class DiscoveryHTTPService(BaseService):
    name = 'discovery_http_service'
    uri = '/services'
    management_config = BaseService.config.get(name, {}).get('management', {})
    discovery = ServiceDiscoveryProvider(**management_config)

    @http('GET', uri)
    def services(self, request):
        """

        :param request:
        :return:
        """
        return json.dumps(self.discovery(vhost=request.args.get('vhost')))

    @http('GET', uri + "/<name>")
    def service_byname(self, request, name):
        """

        :param request:
        :param name:
        :return:
        """
        return json.dumps(self.discovery(name, request.args.get('vhost')))
