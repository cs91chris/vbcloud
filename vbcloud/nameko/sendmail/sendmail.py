import yagmail
from nameko.dependency_providers import Config
from nameko.rpc import rpc


class SendMail:
    name = "sendmail"
    config = Config()

    @rpc
    def send(self, to, subject, contents):
        yag = yagmail.SMTP(config.EMAIL, config.PASSWORD)
        yag.send(
            cc=to.encode('utf-8'),
            subject=subject.encode('utf-8'),
            contents=[contents.encode('utf-8')]
        )
