import logging
from datetime import datetime
from weakref import WeakKeyDictionary

from kombu.messaging import Exchange, Queue
from nameko import events
from nameko.dependency_providers import Config
from nameko.extensions import DependencyProvider
from nameko.messaging import consume, Publisher
from nameko.rpc import rpc


# class LoggingDependency(DependencyProvider):
#     def __init__(self):
#         self.log = logging.getLogger()
#         self.timestamps = WeakKeyDictionary()
#
#     def worker_setup(self, worker_ctx):
#         self.timestamps[worker_ctx] = datetime.now()
#         service_name = worker_ctx.service_name
#         method_name = worker_ctx.entrypoint.method_name
#         self.log.info("Worker %s.%s starting", service_name, method_name)
#
#     def worker_result(self, worker_ctx, result=None, exc_info=None):
#         service_name = worker_ctx.service_name
#         method_name = worker_ctx.entrypoint.method_name
#         status = "completed" if exc_info is None else "errored"
#         worker_started = self.timestamps.pop(worker_ctx)
#         elapsed = (datetime.now() - worker_started).seconds
#         self.log.info("Worker %s.%s %s after %ss", service_name, method_name, status, elapsed)
#
#
# class EmitterService:
#     name = 'emitter_service'
#     event_name = 'some_event'
#     dispatch = events.EventDispatcher()
#
#     @rpc
#     def emit(self):
#         # noinspection PyCallingNonCallable
#         self.dispatch(self.event_name, 'payload')
#
#
# class HandlerService:
#     name = 'handler_service'
#
#     @events.event_handler('emitter_service', 'some_event')
#     def handler(self, payload):
#         print('%s : %s' % (self.name, payload))
#
#
# class BaseService:
#     config = Config()
#     logger = LoggingDependency()
#     monitoring_exchange = Exchange(name="monitoring")
#     ping_queue = Queue(name="<unique-name>", routing_key="ping", exchange=monitoring_exchange.name)
#
#     @consume(queue=ping_queue)
#     def handle_ping(self, payload):
#         print(f"consumed payload: {payload}")
#
#
# class Slack(BaseService):
#     name = 'slackbot'
#
#     publish = Publisher(exchange=BaseService.monitoring_exchange)
#
#     @rpc
#     def on_status(self, message):
#         # noinspection PyCallingNonCallable
#         self.publish(message, routing_key="ping")
#
#
# class ConsumerService(BaseService):
#     name = "consumer"

from nameko.timer import timer
from nameko import config
from apscheduler.schedulers.background import BlockingScheduler
from nameko.standalone.rpc import ClusterRpcProxy


class SchedulerProvider(DependencyProvider):
    def __init__(self, conf, func=None):
        self.conf = conf
        jobs = self.conf['SCHEDULER'].pop('JOBS', {})
        self.scheduler = BlockingScheduler(self.conf['SCHEDULER'] or {})

        for j in jobs:
            self.scheduler.add_job(func=func or self.invoke, **j)

        self.scheduler.start()

    def invoke(self, service, action, **kwargs):
        with ClusterRpcProxy(self.conf) as client:
            perform = getattr(client[service], action)
            return perform(**kwargs)


class SchedulerService:
    name = "scheduler_service"
    scheduler = SchedulerProvider(config)
