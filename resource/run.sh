#!/bin/bash

SERVICES=${1}
BASE_CONF=${2:-config/config.yaml}


nameko run "${SERVICES}" --config "${BASE_CONF}"
